package com.stormnet.figuresfx.controller;

import com.stormnet.figuresfx.draw.util.Drawer;
import com.stormnet.figuresfx.figures.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class MainScreenViewController implements Initializable {

    private Drawer<Figure> drawer;
    private List<Figure> figures;
    private Random random;

    @FXML
    private Canvas canvas;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        figures = new ArrayList<>();
        random = new Random(System.currentTimeMillis());
        drawer = new Drawer<>(figures);
    }

    private Figure createFigure(double x, double y){
        Figure figure = null;

        switch (random.nextInt(5)){
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, random.nextInt(3), Color.RED, random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, random.nextInt(3), Color.BLUE, random.nextInt(50), random.nextInt(100));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, random.nextInt(3), Color.GREEN, random.nextInt(50), random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_OVAL:
                figure = new Oval(x, y, random.nextInt(3), Color.ORANGE, random.nextInt(50), random.nextInt(50));
                break;
            case Figure.FIGURE_TYPE_SQUARE:
                figure = new Square(x, y, random.nextInt(3), Color.PINK, random.nextInt(50));
                break;
            default:
                System.out.println("Unknown figure!");
        }
        return figure;
    }

    private void repaint(){
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        drawer.draw(canvas.getGraphicsContext2D());
    }

    @FXML
    private void onMouseClicked(MouseEvent mouseEvent) {
        figures.add(createFigure(mouseEvent.getX(), mouseEvent.getY()));
        repaint();
    }
}
