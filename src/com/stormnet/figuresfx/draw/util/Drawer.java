package com.stormnet.figuresfx.draw.util;

import com.stormnet.figuresfx.figures.Figure;
import javafx.scene.canvas.GraphicsContext;

import java.util.List;

public class Drawer < T extends Figure & Drawable> {

    private List<T> figures;

    public Drawer (List <T> figures) {

        this.figures = figures;
    }

    public void draw(GraphicsContext gc) {
        if (figures != null && !figures.isEmpty()) {
            for (T t : figures) {
                t.draw(gc);
            }
        }
    }
}