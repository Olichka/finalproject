package com.stormnet.figuresfx.draw.util;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {

    void draw(GraphicsContext gc);
}