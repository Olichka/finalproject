package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Oval extends Figure {

    private double horizontalRadius;
    private double verticalRadius;

    private Oval(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_OVAL, cx, cy, lineWidth, color);
    }

    public Oval(double cx, double cy, double lineWidth, Color color, double horizontalRadius, double verticalRadius) {
        this(cx, cy, lineWidth, color);
        this.horizontalRadius = horizontalRadius < 10 ? 10 : horizontalRadius;
        this.verticalRadius = verticalRadius < 10 ? 10 : verticalRadius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Oval oval = (Oval) o;
        return Double.compare(oval.horizontalRadius, horizontalRadius) == 0 &&
                Double.compare(oval.verticalRadius, verticalRadius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(horizontalRadius, verticalRadius);
    }

    @Override
    public String toString() {
        return "Oval{" +
                "horizontalRadius=" + horizontalRadius +
                ", verticalRadius=" + verticalRadius +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeOval(cx - horizontalRadius, cy - verticalRadius, horizontalRadius * 2, verticalRadius * 2);
    }
}
