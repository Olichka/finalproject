package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Square extends Figure {

    private double halfBase;

    private Square(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_SQUARE, cx, cy, lineWidth, color);
    }

    public Square(double cx, double cy, double lineWidth, Color color, double halfBase) {
        this(cx, cy, lineWidth, color);
        this.halfBase = halfBase < 10 ? 10 : halfBase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return Double.compare(square.halfBase, halfBase) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(halfBase);
    }

    @Override
    public String toString() {
        return "Square{" +
                "halfBase=" + halfBase +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeRect(cx - halfBase, cy - halfBase, halfBase * 2, halfBase * 2);
    }
}
