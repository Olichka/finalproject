
package com.stormnet.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Triangle extends Figure {
    private double halfHeight;
    private double halfBase;

    private Triangle(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_TRIANGLE, cx, cy, lineWidth, color);
    }

    public Triangle(double cx, double cy, double lineWidth, Color color, double halfHeight, double halfBase) {
        this(cx, cy, lineWidth, color);
        this.halfHeight = halfHeight < 10 ? 10 : halfHeight;
        this.halfBase = halfBase < 10 ? 10 : halfBase;
    }

    public double getHalfHeight() {
        return halfHeight;
    }

    public double getHalfBase() {
        return halfBase;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Triangle triangle = (Triangle) o;
        return Double.compare(triangle.halfHeight, halfHeight) == 0 &&
                Double.compare(triangle.halfBase, halfBase) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(halfHeight, halfBase);
    }

    @Override
    public String toString() {
        return "com.shelpuksoftcorp.figuresfx.figures.Triangle{" +
                "halfHeight=" + halfHeight +
                ", halfBase=" + halfBase +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokeLine(cx - halfBase, cy + halfHeight, cx + halfBase, cy + halfHeight);
        gc.strokeLine(cx + halfBase, cy + halfHeight, cx, cy - halfHeight);
        gc.strokeLine(cx, cy - halfHeight, cx - halfBase, cy + halfHeight);
    }
}

